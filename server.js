const port = process.env.PORT || 1111
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const axios = require('axios');
const flash=require('express-flash-messages');
var session = require('express-session');
const { isAdmin } = require('./form/middlewares')
var sessionStore = new session.MemoryStore;

app.use(session({
    cookie: { maxAge: 24 * 60 * 60 * 1000 },
    store: sessionStore,
    saveUninitialized: true,
    resave: 'true',
    secret: 'secret'
}));
app.use(flash());
app.use(bodyParser.urlencoded({extended: false}))

app.set('view engine','ejs');
app.set('views','./form');


app.get('/admin', function(req,res){
    res.render('loginadmin');
});
app.post('/admin/login', function (req, res) {
    const { username, password  } = req.body;
    axios({
        method: 'post',
        url: 'http://localhost:6000/api/login',
        data: {
            username,
            password
        }
    }).then(data => {  
        req.session.user = {
            token: data.data.tokenadmin
        }
        res.redirect('/homeadmin')
    })
    .catch(err => {
        console.log(err)
        req.flash('notify', 'Please check your username and password ')
        res.redirect("/admin")
    })
})

app.get('/student', function(req,res){
    res.render('loginstudent');
});

app.post('/student/login', function (req, res) {
    const { username, password  } = req.body;
    axios({
        method: 'post',
        url: 'http://localhost:6000/api/loginstudent',
        //  headers: {
        //      "Authorization": "bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MiwidXNlck5hbWUiOiJzdjIiLCJpYXQiOjE1Nzc0MTkxNzgsImV4cCI6MTU3NzQyNjM3OH0.AlQbzmIfs8cRpkBrj9Fq8lKCBz9O1GMt5nVJN4syM00"
        //  },
        data: {
            username,
            password
        }
    }).then(data => {
        console.log(data)      
        res.redirect('/homestudent')
    })
    .catch(err => {
        console.log(err)
        req.flash('notify', 'Please check your username and password ')
        res.redirect("/student")
    })
})

/////////Home/////////////
app.get('/signup', function(req, res){
    res.render('signup');
});

app.post('/student/signup' ,function(req,res) {
    const {name,userName,password,confirmPassword,code} = req.body;
    axios({
        method: 'post',
        url: 'http://localhost:6000/api/user',
        data: {
            name,
            userName,
            password,
            confirmPassword,
            code
        }
    }).then(data => {
        console.log(data)
        req.flash('notify','Sign Up Success')
        res.redirect('/signup')
    }).catch(err => {
        console.log(err)
        req.flash('notify','Sign Up Fail')
        res.redirect('/signup')
    })
})

app.post('/admin/signup' ,function(req,res) {
    const token = req.session.user.token;
    const {userName,password,confirmPassword} = req.body;
    axios({
        method: 'post',
        url: 'http://localhost:6000/api/users',
        headers: {
            "Authorization": "bearer " + token
        },
        data: {
            userName,
            password,   
            confirmPassword,
        }
    }).then(data => {
        console.log(data)
        req.flash('notify','Sign Up Success')
        res.redirect('/admin/admin')
    }).catch(err => {
        console.log(err)
        req.flash('notify','Sign Up Fail')
        res.redirect('/admin/addadmin')
    })
})
app.get('/homeadmin', function(req, res){
    res.render('homeadmin');
});
app.get('/homestudent', function(req, res){
    res.render('homestudent');
});
//////////////Books//////////////////////////


app.get('/admin/book', function(req,res){  
    const token = req.session.user.token;
    console.log(req.query.search)
    axios({
        method: 'get',
        url: `http://localhost:6000/api/admin/books?search= ${req.query.search ? req.query.search.trim():""}`,
        headers: {
            "Authorization": "bearer " + token
        }
    }).then(function(response){
        //console.log(response);
        res.render('./book/books', {
            books: response.data,
            page:req.query.page||1,
            totalpage:5
        });
    })
    .catch(err => {
        
    })
});
app.get('/book/addbook', function(req, res){
    res.render('./book/addbook');
});

app.post('/admin/addbook',function(req,res){
    const {isbn,name,authorId,publisherId,publicationDate,userId,image} = req.body;
    const token = req.session.user.token;
    axios({
        method: 'post',
        url: 'http://localhost:6000/api/admin/books',
        headers: {
            "Authorization": "bearer " + token
                 },
        data: {
                isbn,
                name,
                authorId,
                publisherId,
                publicationDate,
                userId,
                image
              }
    }).then(function(response){
        
        res.redirect("/admin/book")
        console.log(response);
    }).catch(err => {
        req.flash('notify','Insert Fail')
    })
})
app.get('/book/upbook/:id', isAdmin,async function(req, res){
    const token = req.session.user.token;
    const bookId = req.params.id
   const bookdata=await axios({
        method: 'get',
        url: `http://localhost:6000/api/admin/books/${bookId}`,
        headers: {
            "Authorization": "bearer " + token
                 }  
    })
    console.log(bookdata.data)
    res.render('./book/upbook',{
        bookdata: bookdata.data
        
    });
});

app.post('/admin/upbook/:id', isAdmin,  function(req,res){
    const {isbn,name,authorId,publisherId,publicationDate,userId,image} = req.body;
    const token = req.session.user.token;
    const bookId = req.params.id
    axios({
        method: 'put',
        url: `http://localhost:6000/api/books/${bookId}`,     
           headers: {
            "Authorization": "bearer " + token
                 },

        data: { 
                isbn,
                name,
                authorId,
                publisherId,
                publicationDate,
                userId,
                image
              }
    }).then(function(response){
        res.redirect("/admin/book")
        console.log(response);    
       
    }).catch(err => {
        console.log(err)
        req.flash('notify','Update Fall In Love')

    })
})

app.get('/admin/delbook/:id',isAdmin, function(req,res){
    const token = req.session.user.token;
    const bookId = req.params.id
    axios({
        method: 'delete',
        url: `http://localhost:6000/api/books/${bookId}`,
        headers: {
            "Authorization": "bearer " + token
                 },
    }).then(function(response){
        req.flash('notify','Delete Success')
        res.redirect('/admin/book')
        console.log(response);
    }).catch(err => {
        req.flash('notify','Delete Fail')
        res.redirect('/admin/book')
    })
});
/////////////////Publishers////////////////////////
app.get('/admin/publisher', function(req,res){  
    const token = req.session.user.token;
    console.log(req.query.search)
    axios({
        method: 'get',
        url: `http://localhost:6000/api/admin/publishers?search= ${req.query.search ? req.query.search.trim():""}`,
        headers: {
            "Authorization": "bearer " + token
        }
    }).then(function(response){
        console.log(response);
        res.render('./publisher/publisher', {
            publishers: response.data
        });
    })
    .catch(err => {
    })
});
app.get('/publisher/addpublisher', function(req, res){
    res.render('./publisher/addpublisher');
});

app.post('/admin/addpublisher',function(req,res){
    const {name,address,phoneNumber} = req.body;
    const token = req.session.user.token;
    axios({
        method: 'post',
        url: 'http://localhost:6000/api/admin/publishers',
        headers: {
            "Authorization": "bearer " + token
                 },
        data: {
            name,
            address,
            phoneNumber
              }
    }).then(function(response){
        
        res.redirect("/admin/publisher")
        console.log(response);
    }).catch(err => {
        req.flash('notify','Insert Fail')
    })
})
app.get('/publisher/uppublisher/:id', isAdmin,async function(req, res){

    const token = req.session.user.token;
    const publisherId = req.params.id

    const publisherdata=await axios({
        method: 'get',
        url: `http://localhost:6000/api/admin/publishers/${publisherId}`,
        headers: {
            "Authorization": "bearer " + token
                 }  
    })
    console.log(publisherdata.data)
    res.render('./publisher/uppublisher',{
        publisherdata: publisherdata.data
        
    });
});

app.post('/admin/uppublisher/:id', isAdmin,  function(req,res){
    const {name,address,phoneNumber} = req.body;
    const token = req.session.user.token;
    const publisherId = req.params.id
    axios({
        method: 'put',
        url: `http://localhost:6000/api/publishers/${publisherId}`,     
           headers: {
            "Authorization": "bearer " + token
                 },

        data: { 
            name,
            address,
            phoneNumber
              }
    }).then(function(response){
        res.redirect("/admin/publisher")
        console.log(response);    
       
    }).catch(err => {
        req.flash('notify','Update Fall In Love')
    })
})

app.get('/admin/delpublisher/:id',isAdmin, function(req,res){
    const token = req.session.user.token;
    const pubId = req.params.id
    axios({
        method: 'delete',
        url: `http://localhost:6000/api/publishers/${pubId}`,
        headers: {
            "Authorization": "bearer " + token
                 },
    }).then(function(response){
        req.flash('notify','Delete Success')
        res.redirect('/admin/publisher')
        console.log(response);
    }).catch(err => {
        req.flash('notify','Delete Fail')
        res.redirect('/admin/publisher')
    })
});
/////////////////////Authorsss////////////////////////////////
app.get('/admin/author', function(req,res){  
    const token = req.session.user.token;
    console.log(req.query)
    axios({
        method: 'get',
        url: `http://localhost:6000/api/admin/authors?search= ${req.query.search ? req.query.search.trim():""}`,
        headers: {
            "Authorization": "bearer " + token
        }
    }).then(function(response){
        console.log(response);
        res.render('./author/author', {
            authors: response.data
        });
    })
    .catch(err => {
        
    })
});
app.get('/author/addauthor', function(req, res){
    res.render('./author/addauthor');
});

app.post('/admin/addauthor',function(req,res){
    const {name,address,phoneNumber} = req.body;
    const token = req.session.user.token;
    axios({
        method: 'post',
        url: 'http://localhost:6000/api/admin/authors',
        headers: {
            "Authorization": "bearer " + token
                 },
        data: {
            name,
            address,
            phoneNumber
              }
    }).then(function(response){
        
        res.redirect("/admin/author")
        console.log(response);
    }).catch(err => {
        req.flash('notify','Insert Fail')
    })
})
app.get('/author/upauthor/:id', isAdmin,async function(req, res){

    const token = req.session.user.token;
    const authorId = req.params.id

    const authordata=await axios({
        method: 'get',
        url: `http://localhost:6000/api/admin/authors/${authorId}`,
        headers: {
            "Authorization": "bearer " + token
                 }  
    })
    console.log(authordata.data)
    res.render('./author/upauthor',{
        authordata: authordata.data
        
    });
});

app.post('/admin/upauthor/:id', isAdmin,  function(req,res){
    const {name,address,phoneNumber} = req.body;
    const token = req.session.user.token;
    const authorId = req.params.id
    axios({
        method: 'put',
        url: `http://localhost:6000/api/authors/${authorId}`,     
           headers: {
            "Authorization": "bearer " + token
                 },

        data: { 
            name,
            address,
            phoneNumber
              }
    }).then(function(response){
        res.redirect("/admin/author")
        console.log(response);    
       
    }).catch(err => {
        req.flash('notify','Update Fall In Love')

    })
})


app.get('/admin/delauthor/:id',isAdmin, function(req,res){
    const token = req.session.user.token;
    const authorId = req.params.id
    axios({
        method: 'delete',
        url: `http://localhost:6000/api/authors/${authorId}`,
        headers: {
            "Authorization": "bearer " + token
                 },
    }).then(function(response){
        req.flash('notify','Delete Success')
        res.redirect('/admin/author')
        console.log(response);
    }).catch(err => {
        req.flash('notify','Delete Fail')
        res.redirect('/admin/author')
    })
});
///////////////////Student////////////////////////
app.get('/admin/student', function(req,res){  
    const token = req.session.user.token;
    console.log(req.query)
    axios({
        method: 'get',
        url: `http://localhost:6000/api/admin/student?search= ${req.query.search ? req.query.search.trim():""}`,
        headers: {
            "Authorization": "bearer " + token
        }
    }).then(function(response){
        console.log(response);
        res.render('./student/student', {
            students: response.data
        });
    })
    .catch(err => {
   console.log(err)
    })
});
app.get('/admin/upstudent/:id', isAdmin,async function(req, res){

    const token = req.session.user.token;
    const studentid = req.params.id

    const studentdata=await axios({
        method: 'get',
        url: `http://localhost:6000/api/admin/student/${studentid}`,
        headers: {
            "Authorization": "bearer " + token
                 }  
    })
    console.log(studentdata.data)
    res.render('./student/upstudent',{
        studentdata: studentdata.data
        
    });
});

app.post('/admin/upstudent/:id', isAdmin,  function(req,res){
    const {userName,password,name,code} = req.body;
    const token = req.session.user.token;
    const studentId = req.params.id
    axios({
        method: 'put',
        url: `http://localhost:6000/api/admin/${studentId}`,     
           headers: {
            "Authorization": "bearer " + token
                 },

        data: { 
            userName,
            password,
            name,
            code
              }
    }).then(function(response){
        res.redirect("/admin/student")
        console.log(response);    
       
    }).catch(err => {
        console.log(err)
        req.flash('notify','Update Fall In Love')
    })
  })

  app.get('/admin/delstudent/:id',isAdmin, function(req,res){
    const token = req.session.user.token;
    const studentId = req.params.id
    axios({
        method: 'delete',
        url: `http://localhost:6000/api/admin/${studentId}`,
        headers: {
            "Authorization": "bearer " + token
                 },
    }).then(function(response){
        req.flash('notify','Delete Success')
        res.redirect('/admin/student')
        console.log(response);
    }).catch(err => {
        req.flash('notify','Delete Fail')
        res.redirect('/admin/student')
    })
});
////////////Borrow//////////////////

app.get('/admin/borrow', function(req,res){  
    const token = req.session.user.token;
    console.log(req.query)
    axios({
        method: 'get',
        url: `http://localhost:6000/api/admin/borrow?search= ${req.query.search ? req.query.search.trim():""}`,
        headers: {
            "Authorization": "bearer " + token
        }
    }).then(function(response){
        console.log(response);
        res.render('./borrow/borrow', {
            borrows: response.data
        });
    })
    .catch(err => {
        console.log(err)
    })
});
app.get('/borrow/addborrow', function(req, res){
    res.render('./borrow/addborrow');
});

app.post('/admin/addborrow',function(req,res){
    const {studentCode,bookId,borrowDate,bookLender} = req.body;
    const token = req.session.user.token;
    axios({
        method: 'post',
        url: 'http://localhost:6000/api/admin/borrow',
        headers: {
            "Authorization": "bearer " + token
                 },
        data: {
            studentCode,
            bookId,
            borrowDate,
            bookLender
              }
    }).then(function(response){
        res.redirect("/admin/borrow")
        console.log(response);
    }).catch(err => {
        console.log(err)
        req.flash('notify','Insert Fail')
    })
})
app.get('/admin/returnBorrow/:id',isAdmin, function(req,res){
    const token = req.session.user.token;
    const borrowId = req.params.id
    const {returnDate} = res.body;
    axios({
        method: 'put',
        url: `http://localhost:6000/api/admin/borrow/${borrowId}`,
        headers: {
            "Authorization": "bearer " + token
        },
        data:{

            returnDate
        }
    }).then(function(response){
        res.flash("notify","Return Success")
        res.redirect('/admin/borrow')
        console.log(response);
    }).catch(err =>{
        res.flash("notify","Return Fail")
        res.redirect('/admin/publisher')
    })
})

//     const borrowdata= axios({
//         method: 'put',
//         url: `http://localhost:6000/api/admin/borrow/${borrowId}`,
//         headers: {
//             "Authorization": "bearer" + token
//         },

//         data: {         
//             returnDate,
//               }
//     }).then(function(response){
//         res.redirect("/admin/borrow")
//         console.log(response);
//     }).catch(err => {
//         console.log(err)
//         req.flash('notify','Fail')
//     })
// })
app.get('/admin/delborrow/:id',isAdmin, function(req,res){
    const token = req.session.user.token;
    const borrowId = req.params.id
    axios({
        method: 'delete',
        url: `http://localhost:6000/api/borrow/${borrowId}`,
        headers: {
            "Authorization": "bearer " + token
                 },
    }).then(function(response){
        req.flash('notify','Delete Success')
        res.redirect('/admin/borrow')
        console.log(response);
    }).catch(err => {
        req.flash('notify','Delete Fail')
        res.redirect('/admin/borrow')
    })
});
/////////Admin//////////
app.get('/admin/admin', function(req,res){  
    const token = req.session.user.token;
    console.log(req.query)
    axios({
        method: 'get',
        url: `http://localhost:6000/api/users?search= ${req.query.search ? req.query.search.trim():""}`,
        headers: {
            "Authorization": "bearer " + token
        }
    }).then(function(response){
        console.log(response);
        res.render('./admin/admin', {
            admins: response.data
        });
    })
    .catch(err => {
    })
});
app.get('/admin/addadmin', function(req, res){
    res.render('./admin/addadmin');
});

app.get('/admin/upadmin/:id', isAdmin,async function(req, res){

    const token = req.session.user.token;
    const adminId = req.params.id

    const admindata=await axios({
        method: 'get',
        url: `http://localhost:6000/api/users/${adminId}`,
        headers: {
            "Authorization": "bearer " + token
                 }  
    })
    console.log(admindata.data)
    res.render('./admin/upadmin',{
        admindata: admindata.data
        
    });
});

app.post('/admin/upadmin/:id', isAdmin,  function(req,res){
    const {userName,password} = req.body;
    const token = req.session.user.token;
    const adminId = req.params.id
    axios({
        method: 'put',
        url: `http://localhost:6000/api/users/${adminId}`,     
           headers: {
            "Authorization": "bearer " + token
                 },

        data: { 
            userName,
            password,
              }
    }).then(function(response){
        res.redirect("/admin/admin")
        console.log(response);    
       
    }).catch(err => {
        console.log(err)
        req.flash('notify','Update Fall In Love')
    })
  })
  app.get('/admin/deladmin/:id',isAdmin, function(req,res){
    const token = req.session.user.token;
    const adminId = req.params.id
    axios({
        method: 'delete',
        url: `http://localhost:6000/api/borrow/${adminId}`,
        headers: {
            "Authorization": "bearer " + token
                 },
    }).then(function(response){
        req.flash('notify','Delete Success')
        res.redirect('/admin/admin')
        console.log(response);
    }).catch(err => {
        req.flash('notify','Delete Fail')
        res.redirect('/admin/admin')
    })
});

//////////////////////////////////////

app.use('/public', express.static('public'));
app.listen(port)
