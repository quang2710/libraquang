module.exports = {
    isAdmin: (req, res, next) => {
        console.log(req.session)
        if (req.session.user ) {
            next()
        } else {
            req.flash('notify', 'Permission denied')
            res.redirect('/admin')
        }
    }
};